/*
 NotenSchlüssel programm zum Generieren von Punkte schlüsseln
Copyright (C) 2014 Sebastian Rampe

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMenu>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("Noten Schlüssel");

    drawSchluessel();

    connect(ui->actionAbout_Qt,SIGNAL(triggered()),qApp,SLOT(aboutQt()));
    connect(ui->actionAbout,SIGNAL(triggered()),this,SLOT(About()));

    connect(ui->sbPunkte,SIGNAL(valueChanged(double)),this,SLOT(drawSchluessel()));
    connect(ui->sbGrade1,SIGNAL(valueChanged(double)),this,SLOT(drawSchluessel()));
    connect(ui->sbGrade2,SIGNAL(valueChanged(double)),this,SLOT(drawSchluessel()));
    connect(ui->sbGrade3,SIGNAL(valueChanged(double)),this,SLOT(drawSchluessel()));
    connect(ui->sbGrade4,SIGNAL(valueChanged(double)),this,SLOT(drawSchluessel()));
    connect(ui->sbGrade5,SIGNAL(valueChanged(double)),this,SLOT(drawSchluessel()));
    connect(ui->checkBox,SIGNAL(toggled(bool)),       this,SLOT(drawSchluessel()));
    // connect(ui->sbGrade6,SIGNAL(valueChanged(double)),this,SLOT(drawSchluessel()));
    connect(ui->comboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(drawSchluessel()));

    QMenu * nMenuPreset = new QMenu();

    nMenuPreset->addAction("1-4: 15% 5-6: 20%",this,SLOT(loadPreset1()));
    nMenuPreset->addAction("1-4: 12.5% 5-6: 25%",this,SLOT(loadPreset2()));
    nMenuPreset->addAction("1-4: 12.5% 5: 17% 6: 33%",this,SLOT(loadPreset3()));

    ui->pushButton->setMenu(nMenuPreset);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::drawSchluessel()
{
    ui->lblGrade1->setText("100% - " + QString::number(PercentFromGrade(1)) + "%");
    ui->lblGrade2->setText(QString::number(PercentFromGrade(1) - 0.5) + "% - " + QString::number(PercentFromGrade(2)) + "%");
    ui->lblGrade3->setText(QString::number(PercentFromGrade(2) - 0.5) + "% - " + QString::number(PercentFromGrade(3)) + "%");
    ui->lblGrade4->setText(QString::number(PercentFromGrade(3) - 0.5) + "% - " + QString::number(PercentFromGrade(4)) + "%");
    ui->lblGrade5->setText(QString::number(PercentFromGrade(4) - 0.5) + "% - " + QString::number(PercentFromGrade(5)) + "%");
    ui->lblGrade6->setText(QString::number(PercentFromGrade(5) - 0.5) + "% - " + QString::number(PercentFromGrade(6)) + "%");
    ui->sbGrade6->setValue(PercentFromGrade(5));

    int PointMax = FromIntToRep(ui->sbPunkte->value());

    QString nString;
    int Note = 0;
    for(int i = PointMax; i > 0; --i)
    {
        if(Note != getNote(i))
        {
            Note = getNote(i);
            nString.append(QString("------ Note %2 \n%1 - \n").arg(FromRepToString(i).rightJustified(6)).arg(Note));
        }
        else if(Note != getNote(i-1) || ui->checkBox->isChecked())
        {
            nString.append(QString("%1\n").arg(FromRepToString(i).rightJustified(6)));
        }
    }

    ui->textBrowser->setPlainText(nString);
}

void MainWindow::loadPreset1()
{
    ui->sbGrade1->setValue(15.0);
    ui->sbGrade2->setValue(15.0);
    ui->sbGrade3->setValue(15.0);
    ui->sbGrade4->setValue(15.0);
    ui->sbGrade5->setValue(20.0);
}

void MainWindow::loadPreset2()
{
    ui->sbGrade1->setValue(12.5);
    ui->sbGrade2->setValue(12.5);
    ui->sbGrade3->setValue(12.5);
    ui->sbGrade4->setValue(12.5);
    ui->sbGrade5->setValue(25.0);
}

void MainWindow::loadPreset3()
{
    ui->sbGrade1->setValue(12.5);
    ui->sbGrade2->setValue(12.5);
    ui->sbGrade3->setValue(12.5);
    ui->sbGrade4->setValue(12.5);
    ui->sbGrade5->setValue(17.0);
}

void MainWindow::About()
{
    QMessageBox::about(this,"About Noten Schlüssel",
                       "Noten Schlüssel ein Programm zum Erstellen von Punkteschlüsseln\n"
                       "Copyright (C) 2014 Sebastian Rampe\n"
                       "\n"
                       "This program is free software; you can redistribute it and/or\n"
                       "modify it under the terms of the GNU General Public License\n"
                       "as published by the Free Software Foundation; either version 2\n"
                       "of the License, or (at your option) any later version.\n"
                       "\n"
                       "This program is distributed in the hope that it will be useful,\n"
                       "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
                       "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
                       "GNU General Public License for more details.\n"
                       "\n"
                       "You should have received a copy of the GNU General Public License\n"
                       "along with this program; if not, write to the Free Software\n"
                       "Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.\n"
                       "\n"
                       "https://bitbucket.org/zerbp/noten-schluessel\n");
}

int MainWindow::getNote(int iPunkte, int iGrade)
{
    int iGradeValue = PointFromGrade(iGrade);

    if(iGradeValue > iPunkte && iGrade < 6)
        return getNote(iPunkte,iGrade+1);
    return iGrade;
}

int MainWindow::PointFromGrade(int iGrade)
{
    double PointRatio = FromIntToRep(ui->sbPunkte->value()) / 100.0;

    return ceil(PercentFromGrade(iGrade) * PointRatio);
}

double MainWindow::PercentFromGrade(int iGrade)
{
    double iPercent = 100;
    switch(iGrade)
    {
    case 1:
        iPercent -= ui->sbGrade1->value();
        break;
    case 2:
        iPercent -= ui->sbGrade2->value() + ui->sbGrade1->value();
        break;
    case 3:
        iPercent -= ui->sbGrade3->value() + ui->sbGrade2->value() + ui->sbGrade1->value();
        break;
    case 4:
        iPercent -= ui->sbGrade4->value() + ui->sbGrade3->value() + ui->sbGrade2->value() + ui->sbGrade1->value();
        break;
    case 5:
        iPercent -= ui->sbGrade5->value() + ui->sbGrade4->value() + ui->sbGrade3->value() + ui->sbGrade2->value() + ui->sbGrade1->value();
        break;
    case 6:
        iPercent = 0;
        break;
    }

    return qMax(0.0,iPercent);
}

QString MainWindow::FromRepToString(int pointRep)
{
    if(ui->comboBox->currentIndex() == 0)
    {
        return QString::number(static_cast<double>(pointRep) / 2.0,'f',1);
    }
    return QString::number(static_cast<double>(pointRep),'f',1);
}

int MainWindow::FromIntToRep(double iPunte)
{
    if(ui->comboBox->currentIndex() == 0)
    {
        return iPunte*2;
    }
    return iPunte;
}
