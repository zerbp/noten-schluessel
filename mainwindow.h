/*
 NotenSchlüssel programm zum Generieren von Punkte schlüsseln
Copyright (C) 2014 Sebastian Rampe

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void drawSchluessel();

    void loadPreset1();
    void loadPreset2();
    void loadPreset3();

    void About();
private:
    Ui::MainWindow *ui;

    int getNote(int iPunkte,int iGrade = 1);
    int PointFromGrade(int iGrade);
    double PercentFromGrade(int iGrade);

    QString FromRepToString(int pointRep);
    int FromIntToRep(double points);
};

#endif // MAINWINDOW_H
